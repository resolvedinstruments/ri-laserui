

import sys
import os

QML_DIR = os.path.join(os.path.dirname(__file__), "qml")
def qml_dir(d):
    return os.path.join(QML_DIR, d)

# import qt from qtpy for both PySide2/PyQt5 compatability
import qtpy
from qtpy import QtGui, QtWidgets, QtCore, QtQml
from qtpy.QtCore import Signal, Slot, Property
from qtpy.QtCore import qDebug, QUrl, QObject, QTimer

# print(f"Using {qtpy.API_NAME} API")

flush = sys.stdout.flush
flush() # for above prints


import numpy as np

class FileFinder(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

    @Slot(str, result=QUrl)
    def find(self, path):
        return QUrl.fromLocalFile(qml_dir(path))

# Finally register the type so it can be imported from QML
# QtQml.qmlRegisterType(Test, "LaserUI", 1, 0, "Test")
QtQml.qmlRegisterType(FileFinder, "LaserUI", 1, 0, "FileFinder")


from .qlaser import QLaserController
QtQml.qmlRegisterType(QLaserController, "LaserUI", 1, 0, "LaserController")

from .qmatplotlib import QMPLFigure
QtQml.qmlRegisterType(QMPLFigure, "LaserUI", 1, 0, "QMPLFigure")

from .qheater import QHeater
QtQml.qmlRegisterType(QHeater, "LaserUI", 1, 0, "Heater")

from .datalogger import DataLogger, LaserPlot
QtQml.qmlRegisterType(DataLogger, "LaserUI", 1, 0, "DataLogger")
QtQml.qmlRegisterType(LaserPlot, "LaserUI", 1, 0, "LaserPlot")


def run():
    app = QtGui.QGuiApplication(sys.argv)

    engine = QtQml.QQmlApplicationEngine(app)
    # engine.setBaseUrl(QUrl.fromLocalFile(qml_dir("")))
    # print(f"baseurl: {engine.baseUrl()}")
    flush()
    engine.load(qml_dir("main.qml"))
    # engine.load("main.qml")


    timer = QtCore.QTimer()
    timer.timeout.connect(lambda: None)
    timer.start(100)

    app.exec_()


if __name__ == '__main__':
    run()
