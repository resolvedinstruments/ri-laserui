

import qtpy
# from qtpy import QtGui, QtWidgets, QtCore, QtQml
from qtpy.QtCore import QObject, Signal, Slot, Property
from qtpy.QtCore import qDebug
from qtpy.QtCore import QStringListModel, QTimer


import serial
import serial.tools.list_ports



class QLaserController(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)

        self._ser = None

        self._ports = []
        self._qports = QStringListModel(["thing"])
        self.update_ports()

        self._timer = QTimer(self)
        self._timer.timeout.connect(self.update)

        self._laser1 = QLaser(self, laserstr="980")
        self._laser2 = QLaser(self, laserstr="1550")
        self._tec1 = QTempTEC(self, 1)
        self._tec2 = QTempTEC(self, 2)

        self._ramp_enabled = False
        self._ramp_rate = 0
        self._laser_pid_enabled = False
        self._laser_pid_target = 0
        self._laser_pid_p = 0
        self._laser_pid_i = 0
        self._laser_pid_d = 0

        self._pdval = 0

        self.destroyed.connect(lambda: self.shutdown())


    # property connected: bool 
    def get_connected(self):
        return self._ser is not None

    connectedChanged = Signal()
    connected = Property(bool, get_connected, notify=connectedChanged)


    @Slot(str)
    def connect(self, port):
        """ Connects to a laser-controller on serial port `port`
        """
        try:
            self._ser = serial.Serial(port, timeout=0.1)
            qDebug("opened port \"%s\"" % port)

            self.check_version()
            self.connectedChanged.emit()
            self.on_connected()
            self._timer.start(300)
        except serial.SerialException:
            qDebug("unable to open port '%s'" % port)
            self._ser = None
            self.connectedChanged.emit()

    def on_connected(self):
        self._get_ramp_rate(update=True)
        self._get_ramp_enabled(update=True)
        self._get_laser_pid_target(update=True)
        self._get_laser_pid_enabled(update=True)
        self._get_laser_pid_p(update=True)
        self._get_laser_pid_i(update=True)
        self._get_laser_pid_d(update=True)


    @Slot(int)
    def connectPort(self, porti):
        """ Connects to a laser-controller on serial port `port`
        """
        port = self._ports[porti].device
        self.connect(port)
    
    @Slot()
    def update(self):
        self.read_pd()
        # self.askf("get_temp_pid_2_output", rtype=float, verbose=True)
        # self.askf("get_rtd_resistance 2", rtype=float, verbose=True)
        # self.askf("get_temp_pid_1_output", rtype=float, verbose=True)
        
        self.laser1.update_current()
        self._tec1.update()
        self._tec2.update()
        
        self.pdvalChanged.emit()

    def pdmv2percent(self, v):
        return v / 25.

    def percent2pdmv(self, v):
        return v * 25.
        
    def read_pd(self):
        mv = self.askf("get_sdadc_mv p", rtype=float, verbose=False)
        self._pdval = self.pdmv2percent(mv) # convert to a percentage of max
        return self._pdval

    def check_version(self):
        """checks to make sure the connected device returns correct version.

        If bad version will disconnect.
        """
        if self._ser is None:
            return
        
        v = self.ask("version")
        if v is None or v.find("Resolved Instruments Laser Controller") != 0:
            qDebug("device returned bad version: %s" % v)
            self.disconnect()
            # self._ser.close()
            # self._ser = None


    @Slot()
    def disconnect(self):
        """ Disconnects laser-controller
        """
        if self._ser is not None:
            self._ser.close()
            self._ser = None
            self._timer.stop()
            self.connectedChanged.emit()

    @Slot()
    def shutdown(self):
        if self._ser is not None:
            self._ser.close()
            self._ser = None


    # property ports: QStringListModel
    def get_ports(self):
        return self._qports

    portsChanged = Signal()
    ports = Property(QStringListModel, get_ports, notify=portsChanged)

    @Slot()
    def update_ports(self):
        self._ports = serial.tools.list_ports.comports()
        pnames = [str(p.description) for p in self._ports]
        if len(pnames) is 0:
            pnames.append("No Ports")
        self._qports.setStringList(pnames)
        self.suggestedPortChanged.emit()

    def _get_suggested_port(self):
        def goodp(p):
            return (p.vid == 0x0483 and p.pid == 0x5740)
        
        goodports = [goodp(p) for p in self._ports]

        try:
            p = goodports.index(True)
        except ValueError:
            p = 0
        return p

    suggestedPortChanged = Signal()
    suggestedPort = Property(int, _get_suggested_port, notify=suggestedPortChanged)


    def ask(self, q, verbose=True):
        if self._ser is None:
            return None

        self._ser.reset_output_buffer()
        self._ser.reset_input_buffer()
        
        qb = q.strip().encode('ascii') + b"\r\n"
        r = None
        
        try:
            tries = 0
            while tries < 3 and r is None:
                tries += 1
                self._ser.write(qb)
                rb = self._ser.readline()
                if qb != rb:
                    print("bad response: %s != %s" % (str(qb), str(rb)))
                else:
                    r = self._ser.readline().decode().strip()
                    if verbose:
                        qDebug("ser: %s -> %s" % (q.strip(), r))
                
        except serial.SerialException:
            qDebug("error communicating with serial port")
            self.disconnect()

        return r

    def askf(self, q, rtype=float, default=0, verbose=True):
        try:
            r = self.ask(q, verbose=verbose)
            r = rtype(r.split()[0])
        except:
            r = default
        return r


    # property laser1: QObject
    def get_laser1(self):
        return self._laser1
    laser1Changed = Signal()
    laser1 = Property(QObject, get_laser1, notify=laser1Changed)

    # property laser1: QObject
    def get_laser2(self):
        return self._laser2

    laser2Changed = Signal()
    laser2 = Property(QObject, get_laser2, notify=laser2Changed)

    # property tec1: QObject
    def get_tec1(self):
        return self._tec1

    tec1Changed = Signal()
    tec1 = Property(QObject, get_tec1, notify=tec1Changed)


    # property tec2: QObject
    def get_tec2(self):
        return self._tec2

    tec2Changed = Signal()
    tec2 = Property(QObject, get_tec2, notify=tec2Changed)

    # property diodeval: float
    def get_pdval(self):
        return self._pdval

    pdvalChanged = Signal()
    pdval = Property(float, get_pdval, notify=pdvalChanged)
    

    def _get_ramp_enabled(self, update=False):
        if update:
            self._ramp_enabled = self.askf("get_ramp_enabled", rtype=int) > 0
            self.rampEnabledChanged.emit()
        return self._ramp_enabled

    def _set_ramp_enabled(self, e):
        e2 = self.askf("set_ramp_enabled %d" % int(e), rtype=int) > 0
        if e2 != self._ramp_enabled:
            self._ramp_enabled = e2
            self.rampEnabledChanged.emit()

    rampEnabledChanged = Signal()
    rampEnabled = Property(bool, _get_ramp_enabled, _set_ramp_enabled, notify=rampEnabledChanged)

    def _get_ramp_rate(self, update=False):
        if update:
            self._ramp_rate = self.askf("get_ramp_rate", rtype=float)
            self.rampRateChanged.emit()
        return self._ramp_rate

    def _set_ramp_rate(self, r):
        r2 = self.askf("set_ramp_rate %f" % float(r), rtype=float)
        if r2 != self._ramp_rate:
            self._ramp_rate = r2
            self.rampRateChanged.emit()

    rampRateChanged = Signal()
    rampRate = Property(float, _get_ramp_rate, _set_ramp_rate, notify=rampRateChanged)


    def _get_laser_pid_enabled(self, update=False):
        if update:
            self._laser_pid_enabled = self.askf("get_laser_pid_enabled", rtype=int) > 0
            self.laserPidEnabledChanged.emit()
        return self._laser_pid_enabled

    def _set_laser_pid_enabled(self, e):
        if e and self.rampEnabled:
            self.rampEnabled = False

        e2 = self.askf("set_laser_pid_enabled %d" % int(e), rtype=int) > 0
        if e2 != self._laser_pid_enabled:
            self._laser_pid_enabled = e2
            self.laserPidEnabledChanged.emit()

    laserPidEnabledChanged = Signal()
    laserPidEnabled = Property(bool, _get_laser_pid_enabled, _set_laser_pid_enabled, notify=laserPidEnabledChanged)


    def _get_laser_pid_target(self, update=False):
        if update:
            v = self.askf("get_laser_pid_target", rtype=float)
            self._laser_pid_target = self.pdmv2percent(v * 1000.)
            self.laserPidTargetChanged.emit()
        return self._laser_pid_target

    def _set_laser_pid_target(self, target):
        v = self.percent2pdmv(target) / 1000.
        v2 = self.askf("set_laser_pid_target %.3f" % v, rtype=float)
        t2 = self.pdmv2percent(v2 * 1000.)
        if t2 != self._laser_pid_target:
            self._laser_pid_target = t2
            self.laserPidTargetChanged.emit()

    laserPidTargetChanged = Signal()
    laserPidTarget = Property(float, _get_laser_pid_target, _set_laser_pid_target, notify=laserPidTargetChanged)


    def _get_laser_pid_p(self, update=False):
        if update:
            self._laser_pid_p = self.askf("get_laser_p", rtype=float)
            self.laserPidPChanged.emit()
        return self._laser_pid_p

    def _set_laser_pid_p(self, r):
        r2 = self.askf("set_laser_p %f" % float(r), rtype=float)
        if r2 != self._laser_pid_p:
            self._laser_pid_p = r2
            self.laserPidPChanged.emit()

    laserPidPChanged = Signal()
    laserPidP = Property(float, _get_laser_pid_p, _set_laser_pid_p, notify=laserPidPChanged)


    def _get_laser_pid_i(self, update=False):
        if update:
            self._laser_pid_i = self.askf("get_laser_i", rtype=float)
            self.laserPidIChanged.emit()
        return self._laser_pid_i

    def _set_laser_pid_i(self, r):
        r2 = self.askf("set_laser_i %f" % float(r), rtype=float)
        if r2 != self._laser_pid_i:
            self._laser_pid_i = r2
            self.laserPidPChanged.emit()

    laserPidIChanged = Signal()
    laserPidI = Property(float, _get_laser_pid_i, _set_laser_pid_i, notify=laserPidIChanged)


    def _get_laser_pid_d(self, update=False):
        if update:
            self._laser_pid_d = self.askf("get_laser_d", rtype=float)
            self.laserPidDChanged.emit()
        return self._laser_pid_d

    def _set_laser_pid_d(self, r):
        r2 = self.askf("set_laser_d %f" % float(r), rtype=float)
        if r2 != self._laser_pid_d:
            self._laser_pid_d = r2
            self.laserPidPChanged.emit()

    laserPidDChanged = Signal()
    laserPidD = Property(float, _get_laser_pid_d, _set_laser_pid_d, notify=laserPidDChanged)








class QLaser(QObject):
    def __init__(self, parent=None, laserstr="1550"):
        super().__init__(parent)
        self._c = parent
        self._laserstr = laserstr

        self._max_current = 40.
        self._current = None

        self._c.connectedChanged.connect(self.on_connect)
        self._c.connectedChanged.connect(self.currentChanged)
        self._c.connectedChanged.connect(self.maxCurrentChanged)


    @Slot()
    def on_connect(self):
        if self._c.connected:
            qDebug("connected :)")

            self._max_current = self._c.askf(
                "get_%s_laser_max" % (self._laserstr), rtype=float)

            self._current = self._c.askf(
                "get_%s_laser_current" % (self._laserstr), rtype=float)
            # get values from board?
        else:
            qDebug("disconnected")

    def get_controller(self):
        return self._c

    controllerChanged = Signal()
    controller = Property(QObject, get_controller, notify=controllerChanged)

    def read_current(self):
        self._current = self._c.askf(
            "get_%s_laser_current" % (self._laserstr,), rtype=float, verbose=False)
        return self._current

    def update_current(self):
        self.read_current()
        self.currentChanged.emit()

    # property current1
    def get_current(self):
        if self._current is None:
            self.read_current()
        
        return self._current
    
    def set_current(self, current):
        current = float(current)
        new_c = self._c.askf(
            "set_%s_laser_current %f" % (self._laserstr, current), rtype=float, verbose=False)
        if new_c != self._current:
            self._current = new_c
            self.currentChanged.emit()

    currentChanged = Signal()
    current = Property(float, get_current, set_current, notify=currentChanged)


    # property current1_max
    def get_maxCurrent(self):
        return float(self._max_current)

    maxCurrentChanged = Signal()
    maxCurrent = Property(float, get_maxCurrent, notify=maxCurrentChanged)









class QTempTEC(QObject):
    def __init__(self, parent=None, num=1):
        if num not in [1, 2]:
            raise RuntimeError("TempTEC num must be 1 or 2.")

        super().__init__(parent)

        self._num = num
        self._c = parent
        self._temp = 0

        self._c.connectedChanged.connect(self.connectedChanged)
        # self._c._timer.timeout.connect(self.update)

        # self.
        # self._c.connectedChanged.connect(self.tempChanged)
        # self._c.connectedChanged.connect(self.setpointChanged)
        # self._c.connectedChanged.connect(self.enabledChanged)

    # property controller: QObject
    def get_controller(self):
        return self._c

    controllerChanged = Signal()
    controller = Property(QObject, get_controller, notify=controllerChanged)

    @Slot()
    def connectedChanged(self):
        if self._c.connected:
            self._temp = 0
            self.setpointChanged.emit()
            self.enabledChanged.emit()
            qDebug("TEC Connected")
        else:
            pass

    @Slot()
    def update(self):
        self._temp = self.read_temp()
        self.tempChanged.emit()


    def read_temp(self):
        return self._c.askf("get_temp_%d" % self._num, rtype=float, verbose=False)

    # property temp: real
    def get_temp(self):
        try:
            return self._temp
        except IndexError:
            return 0.

    tempChanged = Signal()
    temp = Property(float, get_temp, notify=tempChanged)

    # property setpoint: real
    def get_setpoint(self):
        return self._c.askf("get_temp_pid_%d_target" % self._num, default=-1, rtype=float)
    
    def set_setpoint(self, setpoint):
        setpoint = float(setpoint)
        self._c.askf("set_temp_pid_%d_target %.2f" % (self._num, setpoint), default=-1, rtype=float)
        self.setpointChanged.emit()

    setpointChanged = Signal()
    setpoint = Property(float, get_setpoint, set_setpoint, notify=setpointChanged)

    # property enabled: bool
    def get_enabled(self):
        e = self._c.askf("get_temp_pid_%d_enabled" % self._num, 
                    rtype=int, verbose=True) > 0
        return e
    
    def set_enabled(self, enabled):
        enabled = bool(enabled)
        self._c.askf("set_temp_pid_%d_enabled %d" % (self._num, int(enabled)), rtype=bool)
        self.enabledChanged.emit()


    enabledChanged = Signal()
    enabled = Property(bool, get_enabled, set_enabled, notify=enabledChanged)


