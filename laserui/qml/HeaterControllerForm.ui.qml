import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.3

Pane {
    width: 300
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    property alias label: nameText.text
    property alias setpointInput: setpointInput
    property alias tempText: tempText

    //    property alias onSwitch: onSwitch
    property bool small: false
    property bool connected: false

    GridLayout {
        id: gridLayout

        anchors.fill: parent
        anchors.margins: margin

        columns: 2
        flow: GridLayout.LeftToRight
        rowSpacing: 5
        columnSpacing: 0

        Row {
            spacing: 10
            //            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Text {
                id: nameText
                text: "Heater"
                font.pixelSize: small ? 16 : 20
            }

            FaIcon {
                text: fa.fa_exclamation_circle
                color: "#ba4401"
                visible: !connected
                font.pixelSize: small ? 16 : 20
            }
        }

        //        Switch {
        //            id: onSwitch
        //            Layout.alignment: Qt.AlignRight
        //            visible: !small
        //        }

        //        Item {
        //            visible: !small
        //            Layout.preferredWidth: parent.width / 2
        //        }
        Item {
            Layout.preferredWidth: parent.width / 2
        }

        Text {
            text: qsTr("Setpoint:")
            horizontalAlignment: Text.AlignRight
            Layout.alignment: Qt.AlignRight
            font.pixelSize: 12
        }

        Row {
            TextInput {
                id: setpointInput
                text: "23.1"
                font.pixelSize: small ? 14 : 20
                selectByMouse: true
            }

            Text {
                text: " \u00B0" + "C"
                font.pixelSize: small ? 14 : 20
            }

            Layout.alignment: Qt.AlignCenter
        }

        Text {
            text: qsTr("Temperature:")
            font.pixelSize: 12

            Layout.alignment: Qt.AlignRight
        }

        Row {
            Text {
                id: tempText
                text: "23.1"
                font.pixelSize: small ? 14 : 20
            }

            Text {
                text: " \u00B0" + "C"
                font.pixelSize: small ? 14 : 20
            }

            Layout.alignment: Qt.AlignCenter
        }
    }
}
