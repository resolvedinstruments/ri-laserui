import QtQuick 2.0

CurrentControllerForm {
    label: "current"

    QtObject {
        // placeholder laser object until Python QLaser is assigned
        id: nullLaser
        property real current: 0
        property real maxCurrent:  40
        property var controller: QtObject {
            property bool connected: false
        }
    }

    property var laser: nullLaser
    property real oldcurrent: laser.maxCurrent * 0.1;

    connected: laser.controller.connected

    maxText.text: laser.maxCurrent.toFixed(2)

    currentInput.enabled: theController.connected
    currentInput.text: laser.current.toFixed(2)
    currentInput.onAccepted: {
        laser.current = parseFloat(currentInput.text);
        oldcurrent = laser.current;
    }

    slider.enabled: theController.connected
    slider.value: laser.current / laser.maxCurrent
    slider.onMoved: {
        laser.current = laser.maxCurrent * slider.value;
    }

    onSwitch.enabled: theController.connected
    onSwitch.checked: laser.current > 0
    onSwitch.onToggled: {
        if (laser.current > 0) {
            oldcurrent = laser.current;
            laser.current = 0;
        } else {
            if (oldcurrent == 0)
                oldcurrent = laser.maxCurrent * 0.1;
            laser.current = oldcurrent;
        }
    }
}


