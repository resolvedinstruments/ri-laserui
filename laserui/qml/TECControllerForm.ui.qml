import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.3

Pane {
    width: 300
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    property alias label: nameText.text
    property alias setpointInput: setpointInput
    property alias tempText: tempText
    property alias onSwitch: onSwitch

    property bool connected: true
    property bool small: false

    ColumnLayout {
        id: gridLayout
        anchors.fill: parent
        anchors.margins: margin
        spacing: 0

        RowLayout {
            Layout.preferredWidth: parent.width
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Text {
                id: nameText
                text: "1550 TEC"
                font.pixelSize: small ? 16 : 20
            }

            FaIcon {
                text: fa.fa_exclamation_circle
                color: "#ba4401"
                visible: !connected
                font.pixelSize: small ? 16 : 20
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: 5
            }

            Item {
                Layout.fillWidth: true
            }

            Switch {
                id: onSwitch
                Layout.alignment: Qt.AlignRight
                visible: !small
            }
        }

        GridLayout {
            Layout.topMargin: 10
            columnSpacing: 10
            columns: 2

            Text {
                text: qsTr("Setpoint:")
                horizontalAlignment: Text.AlignRight
                Layout.alignment: Qt.AlignRight
                font.pixelSize: 12
                Layout.fillWidth: true
            }

            Row {
                TextInput {
                    id: setpointInput
                    text: "23.1"
                    font.pixelSize: 20
                    selectByMouse: true
                }

                Text {
                    text: " \u00B0" + "C"
                    font.pixelSize: 20
                }

                Layout.alignment: Qt.AlignRight
                Layout.rightMargin: 20
            }

            Text {
                text: qsTr("Temperature:")
                horizontalAlignment: Text.AlignRight
                Layout.alignment: Qt.AlignRight
                font.pixelSize: 12
                Layout.fillWidth: true
            }

            Row {
                Text {
                    id: tempText
                    text: "23.1"
                    font.pixelSize: 20
                }

                Text {
                    text: " \u00B0" + "C"
                    font.pixelSize: 20
                }

                Layout.alignment: Qt.AlignRight
                Layout.rightMargin: 20
            }
        }
    }
}
