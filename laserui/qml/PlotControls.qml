import QtQuick 2.4
import Qt.labs.platform 1.1



PlotControlsForm {
    QtObject {
        id: nullLogger
        property bool running: false
    }

    property var logger: nullLogger

    playButton.onClicked: logger.start()
    stopButton.onClicked: logger.stop()
    isRunning: logger.running

    loadButton.onClicked: loadDialog.open()

    saveButton.onClicked: saveDialog.open()

    FileDialog {
        id: loadDialog

        fileMode: FileDialog.OpenFile
        currentFile: finder.find("main.qml")
        nameFilters: ["CSV or Numpy (*.csv *npy)"]

        onAccepted: {
            logger.load(currentFile);
        }

    }
    FileDialog {
        id: saveDialog

        fileMode: FileDialog.SaveFile
        nameFilters: ["CSV (*.csv)", "Numpy (*.npy)"]

        folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)

        onAccepted: {
            logger.save(currentFile);
        }
    }


}
