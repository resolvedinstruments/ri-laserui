import QtQuick 2.4
import Qt.labs.platform 1.1



ResamplerControlsForm {
    QtObject {
        id: nullLogger
        property bool downsampleEnabled: false
        property real downsampleDt: 1
    }

    property var logger: nullLogger

    enableSwitch.checked: logger.downsampleEnabled
    enableSwitch.onToggled: {
        logger.downsampleEnabled = enableSwitch.checked;
    }

    dtInput.text: logger.downsampleDt.toFixed(1)
    dtInput.onAccepted: {
        logger.downsampleDt = parseFloat(dtInput.text);
    }
}
