import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Pane {
    width: 300
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    property real val: 0
    property string units: "%"
    property alias label: title.text

    property bool connected: true
    property bool small: false

    ColumnLayout {
        id: gridLayout
        anchors.fill: parent
        anchors.margins: margin

        Row {
            spacing: 10

            Text {
                id: title
                text: "Photodiode"
                font.pixelSize: small ? 16 : 20
            }

            FaIcon {
                text: fa.fa_exclamation_circle
                color: "#ba4401"
                visible: !connected
            }
        }

        Row {
            // Text {
            //     text:
            //     font.pixelSize: 24
            // }
            Text {
                text: val.toFixed(3).toString() + " " + units
                font.pixelSize: 24
            }

            Layout.alignment: Qt.AlignCenter

            Layout.margins: 10
        }
    }
}
