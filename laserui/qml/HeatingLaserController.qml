import QtQuick 2.0

HeatingLaserControllerForm {
    label: "current"

    QtObject {
        // placeholder laser object until Python QLaser is assigned
        id: nullLaser
        property real current: 0
        property real maxCurrent:  40
        property var controller: QtObject {
            property bool connected: false
        }
    }

    property var laser: nullLaser
    property real oldcurrent: laser.maxCurrent * 0.1;

    connected: theController.connected
    locked: theController.laserPidEnabled

    maxText.text: laser.maxCurrent.toFixed(2)

    currentInput.enabled: theController.connected
    currentInput.text: laser.current.toFixed(2)
    currentInput.onAccepted: {
        laser.current = parseFloat(currentInput.text);
        oldcurrent = laser.current;
    }

    slider.value: laser.current / laser.maxCurrent
    slider.onMoved: {
        laser.current = laser.maxCurrent * slider.value;
    }

    onSwitch.checked: laser.current > 0
    onSwitch.onToggled: {
        if (laser.current > 0) {
            oldcurrent = laser.current;
            laser.current = 0;
        } else {
            if (oldcurrent == 0)
                oldcurrent = laser.maxCurrent * 0.1;
            laser.current = oldcurrent;
        }
    }

    rampRateInput.text: theController.rampRate.toFixed(3)
    rampRateInput.onAccepted: {
        theController.rampRate = parseFloat(rampRateInput.text);
    }

    rampStartButton.visible: !theController.rampEnabled
    rampStartButton.onClicked: theController.rampEnabled = true;
    rampStopButton.onClicked: theController.rampEnabled = false;

    rampInvertButton.onClicked: {
        theController.rampRate = -theController.rampRate;
    }

    laserLockButton.visible: !theController.laserPidEnabled
    laserLockButton.onClicked: {
        if (lockToPdCheckbox.checked)
            theController.laserPidTarget = theController.pdval;
        else
            theController.laserPidTarget = parseFloat(lockTargetInput.text);
            
        theController.laserPidEnabled = true;
    }
    laserUnlockButton.onClicked: theController.laserPidEnabled = false;

    lockTargetInput.text: theController.pdval.toFixed(3);
    lockToPdCheckbox.onToggled: {
        if (lockToPdCheckbox.checked) {

            lockTargetInput.text = Qt.binding(function() {
                return theController.pdval.toFixed(3);
            });
        } else {
            lockTargetInput.text = theController.pdval.toFixed(3);
        }
    }

    lockTargetInput.onAccepted: {
        theController.laserPidTarget = parseFloat(lockTargetInput.text);
    }

    pInput.text: theController.laserPidP.toFixed(3)
    pInput.onAccepted: theController.laserPidP = parseFloat(pInput.text);
    iInput.text: theController.laserPidI.toFixed(3)
    iInput.onAccepted: theController.laserPidI = parseFloat(iInput.text);
    dInput.text: theController.laserPidD.toFixed(3)
    dInput.onAccepted: theController.laserPidD = parseFloat(dInput.text);



}


