import QtQuick 2.4

TECControllerForm {
    label: "temp"

    QtObject {
        id: nullTEC
        property real temp: 22
        property real setpoint: 30
        property bool enabled: false
        property var controller: QtObject {
            property bool connected: false
        }
    }
    property var tec: nullTEC
    connected: tec.controller.connected

    setpointInput.text: tec.setpoint.toFixed(3)
    setpointInput.onAccepted: {
        tec.setpoint = parseFloat(setpointInput.text);
    }

    tempText.text: tec.temp.toFixed(3)

    onSwitch.checked: tec.enabled
    onSwitch.enabled: theController.connected
    onSwitch.onToggled: {
        tec.enabled = onSwitch.checked;
    }


}
