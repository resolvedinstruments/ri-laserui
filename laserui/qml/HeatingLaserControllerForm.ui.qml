import QtQuick 2.10
import QtQuick.Controls 2.13
import QtQuick.Layouts 1.3

Pane {
    width: 400
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    //    height: 125
    property alias label: title.text
    property alias onSwitch: onSwitch
    property alias slider: slider
    property alias currentInput: currentInput
    property alias maxText: maxText

    property alias rampRateInput: rampRateInput
    property alias rampInvertButton: rampInvertButton
    property alias rampStartButton: rampStartButton
    property alias rampStopButton: rampStopButton

    property alias laserLockButton: laserLockButton
    property alias laserUnlockButton: laserUnlockButton
    property alias lockTargetInput: lockTargetInput
    property alias lockToPdCheckbox: lockToPdCheckbox

    property alias pInput: pInput
    property alias iInput: iInput
    property alias dInput: dInput

    property bool small: false
    property bool connected: false
    property bool locked: false

    ColumnLayout {
        id: gridLayout
        anchors.fill: parent
        spacing: 0
        anchors.margins: margin

        //        columns: 3
        //        flow: GridLayout.LeftToRight
        //        rowSpacing: 0
        //        columnSpacing: 0
        RowLayout {
            Layout.preferredWidth: parent.width
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop

            Text {
                id: title
                text: "laser 1"
                font.pixelSize: small ? 16 : 20
                Layout.alignment: Qt.AlignLeft
            }

            FaIcon {
                text: fa.fa_exclamation_circle
                color: "#ba4401"
                visible: !connected
                font.pixelSize: small ? 16 : 20
                Layout.alignment: Qt.AlignLeft
                Layout.leftMargin: 5
            }

            Item {
                Layout.fillWidth: true
            }

            Switch {
                id: onSwitch
                Layout.alignment: Qt.AlignRight
                visible: !small
                enabled: connected
            }
        }

        Slider {
            id: slider
            enabled: connected & !locked

            Layout.fillWidth: true
        }

        RowLayout {
            //            width: parent.widths
            //            Layout.fillWidth: true
            Layout.preferredWidth: parent.width

            //            Layout.fillWidth: true
            Text {
                Layout.preferredWidth: maxTextRow.width
                text: "0 mA"
                font.pixelSize: small ? 12 : 14

                //            Layout.preferredWidth: parent.width / 3
                Layout.alignment: Qt.AlignLeft
            }

            Row {
                TextInput {
                    id: currentInput
                    text: "10"
                    font.pixelSize: small ? 12 : 18
                    selectByMouse: true
                    enabled: connected & !locked
                }

                Text {
                    text: " mA"
                    font.pixelSize: small ? 12 : 18
                }

                Layout.alignment: Qt.AlignCenter
            }

            Row {
                id: maxTextRow
                Text {
                    id: maxText
                    text: "40"
                    font.pixelSize: small ? 12 : 14
                }
                Text {
                    text: " mA"
                    font.pixelSize: small ? 12 : 14
                }

                Layout.alignment: Qt.AlignRight
            }
        }

        GridLayout {
            //            Layout.fillWidth: true
            Layout.preferredWidth: parent.width
            Layout.topMargin: 10
            columns: 5

            Text {
                text: "Ramp:"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                enabled: connected
            }

            TextField {
                id: rampRateInput
                //                Layout.fillWidth: true
                text: "10"
                horizontalAlignment: Text.AlignRight
                Layout.preferredWidth: 100
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
            }

            Text {
                text: "mA/s"
                font.pixelSize: small ? 12 : 18

                Layout.rightMargin: 10
            }

            Button {
                id: rampInvertButton
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40
                contentItem: FaIcon {
                    text: fa.fa_exchange
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                enabled: connected & !locked
                Layout.alignment: Qt.AlignCenter
            }

            Button {
                id: rampStartButton
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40

                contentItem: FaIcon {
                    text: fa.fa_play
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                enabled: connected & !locked
            }
            Button {
                id: rampStopButton
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40

                contentItem: FaIcon {
                    text: fa.fa_stop
                    color: enabled ? activePallete.text : disabledPallete.text
                }
                visible: !rampStartButton.visible
                enabled: connected & !locked
            }

            Text {
                text: "Target:"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
            }

            TextField {
                id: lockTargetInput
                //                Layout.fillWidth: true
                Layout.preferredWidth: 100
                text: "10"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
                enabled: connected & !lockToPdCheckbox.checked
            }

            Text {
                text: "%"
                font.pixelSize: small ? 12 : 18
                Layout.rightMargin: 10
            }

            CheckBox {
                id: lockToPdCheckbox
                text: ""
                checked: true
                enabled: connected
                Layout.alignment: Qt.AlignHCenter
            }

            Button {
                id: laserLockButton
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40

                contentItem: FaIcon {
                    text: fa.fa_lock
                    color: enabled ? activePallete.text : disabledPallete.text
                }
                enabled: connected
            }
            Button {
                id: laserUnlockButton
                Layout.preferredWidth: 40
                Layout.preferredHeight: 40

                contentItem: FaIcon {
                    text: fa.fa_stop
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                enabled: connected
                visible: !laserLockButton.visible
            }
        } // Ramp/Target Grid

        RowLayout {
            Layout.topMargin: 10
            visible: !small

            Text {
                text: "P:"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
            }

            TextField {
                id: pInput
                //                Layout.fillWidth: true
                Layout.preferredWidth: 80
                text: "10"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
                enabled: connected //& !locked
            }
            // SpinBox {
            //     id: pInput
            //     //                Layout.fillWidth: true
            //     Layout.preferredWidth: 80
            //     value: 10
            //     editable: true

            //     horizontalAlignment: Text.AlignRight
            //     font.pixelSize: small ? 12 : 18
            //     selectByMouse: true
            //     enabled: connected & !locked
            // }

            Text {
                text: "I:"
                Layout.fillWidth: true
                // Layout.preferredHeight: 10
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
            }

            TextField {
                id: iInput
                //                Layout.fillWidth: true
                Layout.preferredWidth: 80
                text: "10"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
                enabled: connected //& !locked
            }

            Text {
                text: "D:"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
            }

            TextField {
                id: dInput
                //                Layout.fillWidth: true
                Layout.preferredWidth: 80
                text: "10"
                horizontalAlignment: Text.AlignRight
                font.pixelSize: small ? 12 : 18
                selectByMouse: true
                enabled: connected //& !locked
            }
        }
    }
}
