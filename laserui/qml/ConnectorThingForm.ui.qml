import QtQuick 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.13

Pane {
    width: 600
    property int margin: 5
    height: gridLayout.implicitHeight + 2 * margin

    property alias label: title.text
    property bool connected: false
    property alias refreshButton: refreshButton
    property alias portCombo: portCombo
    property alias connectButton: connectButton
    property alias disconnectButton: disconnectButton

    ColumnLayout {
        id: gridLayout
        anchors.fill: parent
        anchors.margins: margin

        Text {
            id: title
            text: "Connect"
            font.pixelSize: 20
        }

        RowLayout {
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignCenter

            //            Layout.margins: 10
            Button {
                id: refreshButton
                contentItem: FaIcon {
                    text: fa.fa_sync
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                enabled: !connected
            }

            ComboBox {
                id: portCombo
                textRole: "display"

                enabled: !connected
                Layout.fillWidth: true
            }

            Button {
                id: connectButton
                contentItem: FaIcon {
                    text: fa.fa_bolt
                    color: enabled ? activePallete.text : disabledPallete.text
                }

                enabled: portCombo.currentText !== "No Ports"
                visible: !connected
            }

            Button {
                id: disconnectButton
                contentItem: FaIcon {
                    text: fa.fa_times
                    color: enabled ? activePallete.text : disabledPallete.text
                }
                visible: connected
            }
        }
    }
}
