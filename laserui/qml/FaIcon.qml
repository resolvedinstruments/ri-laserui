import QtQuick 2.10

// followed guide here:
//   http://kdeblog.mageprojects.com/2012/11/20/using-fonts-awesome-in-qml/
// need to load font with a FontLoader,
// find unicode values from font-awesome.css:
//   https://github.com/FortAwesome/Font-Awesome/blob/master/css/font-awesome.css

Text {
    id: fatext
    property alias unicode: fatext.text
    property alias size: fatext.font.pixelSize

    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter


//    anchors.centerIn: parent
    font.family: awesomesolid.name //"fa=solid-900" //
    font.pixelSize: 20
    text: ""
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
