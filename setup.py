#!/usr/bin/env python

from distutils.core import setup

# Check if PySide2 is installed.  If so, require it instead of PyQt5
try:
      import PySide2
      qtdep = ['PySide2']
except ModuleNotFoundError:
      qtdep = ['PyQt5']

setup(name='laserui',
      version='0.2',
      author='Callum Doolin',
      author_email='callum@resolvedinstruments.com',
      packages=['laserui'],
      package_data={
            'laserui': ['qml/*.qml', 'qml/*.ttf'],
      },

      # create laser-controller-ui script to run
      entry_points = {
            'console_scripts': [
                  'laserui = laserui.main:run',
            ]
      },

      # require PyQt5, but should figure out way to make it optional if PySide2 is found
      install_requires = qtdep + [
            'qtpy',
            'pyserial',
            'minimalmodbus',
            'matplotlib',
            'numpy',
      ]
)
     